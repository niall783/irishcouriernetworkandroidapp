package com.thetaverde.niall.irishcouriernetwork;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.locks.AbstractOwnableSynchronizer;

import javax.net.ssl.HttpsURLConnection;

public class EnterAddressActivity extends AppCompatActivity {



    EditText addressLineOneET;
    EditText addressLineTwoET;
    EditText townCityET;
    EditText countyET;

    String addressFromUser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_address);

        addressLineOneET = findViewById(R.id.addressLineOneET);
        addressLineTwoET = findViewById(R.id.addressLineTwoET);
        townCityET = findViewById(R.id.townCityET);
        countyET = findViewById(R.id.countyET);

    }


    public void saveAddress(View view){

        String addressLineOne = String.valueOf(addressLineOneET.getText());
        String addressLineTwo = String.valueOf(addressLineTwoET.getText());
        String townCity = String.valueOf(townCityET.getText());
        String county = String.valueOf(countyET.getText());


        if (!addressLineOne.equals("") || !addressLineTwo.equals("")) {

            addressFromUser = addressLineOne + " " + addressLineTwo + " " + townCity + " " + county;

            Log.i("AddressUser", addressFromUser);

            MainActivity.sharedPreferences.edit().putString("HomeAddress", addressFromUser).apply();

            String latLng = geoCodeAddress(addressFromUser);

            Log.i("GeoCodedAddress", latLng);


            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            finish();

        }else{

            Toast.makeText(this, "Please fill in your address.", Toast.LENGTH_SHORT).show();

        }



    }


    public void clear(View view){

        addressLineOneET.setText("");
        addressLineTwoET.setText("");
        townCityET.setText("");
        countyET.setText("");

    }








        public String geoCodeAddress(String address) {

            String text = null;

            try {
            // Defined URL  where to send data
            //URL url = new URL("https://irishcouriernetwork.herokuapp.com/api/addresses/geocode/?type=post");
                URL url = new URL("https://irishcouriernetwork.herokuapp.com/api/addresses/geocode/");


            String dataString =
                    URLEncoder.encode("address", "UTF-8") + "=" + URLEncoder.encode(address, "UTF-8");

            // Send POST data request
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setDoOutput(true);
            OutputStream outputStream = httpsURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

//                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//                httpURLConnection.setRequestMethod("POST");
//                httpURLConnection.setDoOutput(true);
//                OutputStream outputStream = httpURLConnection.getOutputStream();
//                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));


            bufferedWriter.write(dataString);
            bufferedWriter.flush();
            bufferedWriter.close();


//            InputStream inputStream = httpURLConnection.getInputStream();
//            inputStream.close();
//            httpURLConnection.disconnect();

            InputStream inputStream = httpsURLConnection.getInputStream();
            inputStream.close();
            httpsURLConnection.disconnect();



//            // Get the server response
//            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//
//            // Read Server Response
//            while ((line = reader.readLine()) != null) {
//                // Append server response in string
//                sb.append(line + "\n");
//            }
//
//
//            text = sb.toString();


            } catch (IOException ex) {
                ex.printStackTrace();
            }


            return text;
        }


































}
