package com.thetaverde.niall.irishcouriernetwork;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.time.OffsetDateTime;
import java.util.ArrayList;



    public class LoadsRecyclerViewAdapter extends RecyclerView.Adapter<LoadsRecyclerViewAdapter.MyViewHolder> {


        ArrayList<RecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();

        Context context;

        LoadsRecyclerViewAdapter(ArrayList<RecyclerViewItem> recyclerViewItemsArrayList, Context context){

            this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;

            this.context = context;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row,parent,false);

            return new MyViewHolder(view,context,recyclerViewItemsArrayList);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {


            holder.recyclerViewloadIdTV.setText(recyclerViewItemsArrayList.get(position).getLoadId());

            holder.clientNameTV.setText(recyclerViewItemsArrayList.get(position).getClientNameTV());


            holder.timeStamp.setText(parseDateTime(recyclerViewItemsArrayList.get(position).getTimeStamp()));




            holder.dropOfTimeTV.setText(parseDateTime(recyclerViewItemsArrayList.get(position).getDropOfTimeTV()));
            holder.dropOfLocTV.setText(recyclerViewItemsArrayList.get(position).getDropOfLocTV());

            holder.pickUpTimeTV.setText(parseDateTime(recyclerViewItemsArrayList.get(position).getPickUpTimeTV()));
            holder.pickUpLocTV.setText(recyclerViewItemsArrayList.get(position).getPickUpLocTV());

            holder.vanTypeTV.setText(recyclerViewItemsArrayList.get(position).getVanTypeTV());

            holder.distFromCurrentLocationTV.setText(recyclerViewItemsArrayList.get(position).getDistFromPickUpToLoc1TV());


            //holder.caveName.setText(recyclerViewItemsArrayList.get(position.getCaveName());
            //holder.caveCountry.setText(recyclerViewItemsArrayList.get(position).getCaveCountry());

        }

        @Override
        public int getItemCount() {
            return recyclerViewItemsArrayList.size();
        }





        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            //TextView caveName;
            //TextView caveCountry;

            TextView recyclerViewloadIdTV;

            TextView clientNameTV;

            TextView timeStamp;
            TextView dropOfTimeTV;
            TextView dropOfLocTV;
            TextView pickUpTimeTV;
            TextView pickUpLocTV;
            TextView vanTypeTV;

            TextView distFromCurrentLocationTV;


            ArrayList<RecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();
            Context context;


            public MyViewHolder(View itemView, Context context, ArrayList<RecyclerViewItem> recyclerViewItemsArrayList) {
                super(itemView);

                itemView.setOnClickListener(this);

                this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;
                this.context = context;

                //caveName = (TextView) itemView.findViewById(R.id.caveNameRecyclerViewTextView);
                //caveCountry = (TextView) itemView.findViewById(R.id.caveCountryRecyclerViewTextView);

                recyclerViewloadIdTV = (TextView) itemView.findViewById(R.id.recyclerViewloadIdTV);

                clientNameTV = (TextView) itemView.findViewById(R.id.RVclientNameTV);

                timeStamp = (TextView) itemView.findViewById(R.id.RVtimeStampTV);
                dropOfTimeTV = (TextView) itemView.findViewById(R.id.RVdropOfTimeTV);
                dropOfLocTV = (TextView) itemView.findViewById(R.id.RVdropOfLocTV);
                pickUpTimeTV = (TextView) itemView.findViewById(R.id.RVpickUpTimeTV);
                pickUpLocTV = (TextView) itemView.findViewById(R.id.RVpickUpLocTV);
                vanTypeTV = (TextView) itemView.findViewById(R.id.RVvanTypeTV);

                distFromCurrentLocationTV = itemView.findViewById(R.id.distFromPickUpToLoc1TV);


            }

            @Override
            public void onClick(View v) {

                int position = getAdapterPosition();

                RecyclerViewItem recyclerViewItem = this.recyclerViewItemsArrayList.get(position);

//                //resetting the jsonOrder
//                MainActivity.sharedPreferences.edit().putString("jsonOrders","").apply();


                Intent viewLoad = new Intent(this.context,LoadActivity.class);
                viewLoad.putExtra("LoadObject", recyclerViewItem.getLoadClass()); //passing the entire caveDTO through to the next activity
                this.context.startActivity(viewLoad);

//                this finishes the recyclerView Activity when the user selects their cave and returns to the sendCallOutActivity
//                ((Activity)context).finish();



            }
        }


        public void setFilter(ArrayList<RecyclerViewItem> newList){


            recyclerViewItemsArrayList.clear();
            recyclerViewItemsArrayList.addAll(newList);
            notifyDataSetChanged();

        }



        public String parseDateTime(String dateTime){


            OffsetDateTime odt = OffsetDateTime.parse(dateTime);

//            Log.i("DayOfWeek", String.valueOf(odt.getDayOfWeek()));
//            Log.i("LocalDateTime", String.valueOf(odt.toLocalDateTime()));
//            Log.i("LocalTime", String.valueOf(odt.toLocalTime()));
//            Log.i("OffsetTime", String.valueOf(odt.toOffsetTime()));
//            Log.i("ZonedDateTime", String.valueOf(odt.toZonedDateTime()));
//            Log.i("LocalDate", String.valueOf(odt.toLocalDate()));
//            Log.i("ZonedDateTime", String.valueOf(odt.toZonedDateTime()));
//            Log.i("ZonedDateTime", String.valueOf(odt.toZonedDateTime()));
//            Log.i("CustomTime", String.valueOf(odt.getHour()) + ":" + String.valueOf(odt.getMinute()));
//            Log.i("CustomDate", String.valueOf(odt.getDayOfMonth()) + "-" + String.valueOf(odt.getDayOfMonth()) + "-" + String.valueOf(odt.getYear()));


            String newTime = odt.getHour() + ":" + odt.getMinute();

            //when the minute is less that 10 is prints 13:5 instead of 13:05
            int minute = odt.getMinute();
            if (minute < 10){

                newTime = odt.getHour() + ":0" + odt.getMinute();

            }

            String newDate = odt.getDayOfMonth() + " " + odt.getMonth();


            return newTime + " | " + newDate;

        }







    }