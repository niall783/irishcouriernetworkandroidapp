package com.thetaverde.niall.irishcouriernetwork;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {


    TextView textView;

    static SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Irish Courier Network");


        textView = (TextView) findViewById(R.id.text);

        //initializing shared preferences
        sharedPreferences = MainActivity.this.getSharedPreferences("com.thetaverde.niall.irishcouriernetwork", Context.MODE_PRIVATE);


        sharedPreferences.edit().putString("HomeLat","53.3498").apply();
        sharedPreferences.edit().putString("HomeLong","-6.2603").apply();





        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://irishcouriernetwork.herokuapp.com/api/loads/?format=json";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        textView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("That didn't work!");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);




    }


    public void viewLoads(View view){

        sharedPreferences.edit().putString("GoingHome","True").apply();
        Intent intent = new Intent(this,LoadsRecyclerView.class);
        intent.putExtra("sourceActivity", "MainActivity");
        startActivity(intent);

    }

    public void viewLoadsToGoHome(View view){

        sharedPreferences.edit().putString("GoingHome","True").apply();
        Intent intent = new Intent(this,LoadsRecyclerView.class);
        intent.putExtra("sourceActivity", "MainActivity");
        startActivity(intent);

    }

    public void settingsActivity(View view){

        Intent intent = new Intent(this,SettingsActivity.class);
        startActivity(intent);

    }



}
