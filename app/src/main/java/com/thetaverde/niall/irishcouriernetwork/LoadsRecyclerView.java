package com.thetaverde.niall.irishcouriernetwork;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class LoadsRecyclerView extends AppCompatActivity {

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_loads_recycler_view);
//    }






    ConstraintLayout constraintLayout;




    TextView pickUpLocationTV;
    TextView dropOffLocationTV;

    TextView noLoadsTV;

    DatePickerDialog picker;
    EditText pickUpDateET;

    Intent intent;
    String sourceActivity;




    LocationManager locationManager;
    LocationListener locationListener;



    Toolbar recyclerViewSearchToolBar;
    RecyclerView recyclerView;
    LoadsRecyclerViewAdapter loadsRecyclerViewAdapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<RecyclerViewItem> recyclerViewArrayList;

    ArrayList<LoadClass> loadsArrayList;




    Location usersLocation;

    //i am required to have this
    // the request code is the number that i set and for 'Access fine location' it is equal to 1.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //a request code of 1 is the 'access fine location' request
        if (grantResults.length > 0 && requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            //i am required to check to see if i have permission here
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Log.i("Request Granted", "Yes");

            }
        }


    }







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loads_recycler_view);

        setTitle("Loads");


        intent = getIntent();

        sourceActivity = intent.getStringExtra("sourceActivity");

//        recyclerViewSearchToolBar = (Toolbar) findViewById(R.id.recyclerViewSearchToolbar);
        //setSupportActionBar(recyclerViewSearchToolBar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        constraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayoutLoadsRecyclerView);


        pickUpLocationTV = findViewById(R.id.pickUpLocationTV);
        dropOffLocationTV = findViewById(R.id.dropOffLocationTV);

        noLoadsTV = findViewById(R.id.noLoadsTV);
        noLoadsTV.setVisibility(View.INVISIBLE);












        pickUpDateET=(EditText) findViewById(R.id.pickUpDateET);
        pickUpDateET.setInputType(InputType.TYPE_NULL);
        pickUpDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(LoadsRecyclerView.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                ArrayList months = new ArrayList();
                                months.add("Jan");
                                months.add("Feb");
                                months.add("Mar");
                                months.add("Apr");
                                months.add("May");
                                months.add("Jun");
                                months.add("Jul");
                                months.add("Aug");
                                months.add("Sep");
                                months.add("Oct");
                                months.add("Nov");
                                months.add("Dec");

                                String dateToDisplayToUser = dayOfMonth + " " + months.get(monthOfYear) + " " + year;

                                //pickUpDateET.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                pickUpDateET.setText(dateToDisplayToUser);





                                String dateToSendToServer = year + "-" + parseMonthAndDay(monthOfYear + 1) + "-" + parseMonthAndDay(dayOfMonth);
                                MainActivity.sharedPreferences.edit().putString("pickUpDate",dateToSendToServer).apply();

                                downloadNewLoads("");

                            }
                        }, year, month, day);
                picker.show();
            }
        });





















        recyclerViewArrayList = new ArrayList<>();

        loadsArrayList = new ArrayList<>();







        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                //if the user location change this variable will record the current value
                usersLocation = location;

                Log.i("LocationChanged", location.toString());


                locationManager.removeUpdates(this);
                // TODO: 30/08/2018 add this everywhere that it is necessary


            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };


        //if device is running SDK < 23
        if (Build.VERSION.SDK_INT < 23) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        } else {

            //if we do not have permission
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                //Asking for permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if(lastKnownLocation != null){
                    usersLocation = lastKnownLocation;
                }
            }

        }




        Log.i("OnCreate", "Activated");
        downloadNewLoads(sourceActivity);



//        loadsRecyclerViewAdapter = new LoadsRecyclerViewAdapter(recyclerViewArrayList,this);
//
//        recyclerView.setAdapter(loadsRecyclerViewAdapter);



    }



















    public void downloadNewLoads(String source){

        //Log.i("DownloadNewLoads ", "Activated");

        String dist1 = "null";
        String dist2 = "null";

        //if the user has navigated to activity from another activity other that the PickUpDropOffActivity then reset PickUpName and DropOffName
        if (source.equals("MainActivity")){
            MainActivity.sharedPreferences.edit().putString("PickUpName", "My Location").apply();
            MainActivity.sharedPreferences.edit().putString("DropOffName", "Anywhere").apply();
            MainActivity.sharedPreferences.edit().putString("pickUpDate", "Any Date").apply();
        }


        String pickUpLat = MainActivity.sharedPreferences.getString("PickUpLat","null");
        String pickUpLng = MainActivity.sharedPreferences.getString("PickUpLng","null");
        String pickUpName = MainActivity.sharedPreferences.getString("PickUpName","My Location");

        String dropOffLat = MainActivity.sharedPreferences.getString("DropOffLat","null");
        String dropOffLng = MainActivity.sharedPreferences.getString("DropOffLng","null");
        String dropOffName = MainActivity.sharedPreferences.getString("DropOffName","Anywhere");

        String pickUpDate = MainActivity.sharedPreferences.getString("pickUpDate", "Any Date");




        pickUpLocationTV.setText(pickUpName);
        dropOffLocationTV.setText(dropOffName);
        //pickUpDateET.setText(pickUpDate);


        if (pickUpName.equals("My Location")){
            //Getting the user's location
            getUserLocation();

            if (usersLocation != null){
                pickUpLat = String.valueOf(usersLocation.getLatitude());
                pickUpLng = String.valueOf(usersLocation.getLongitude());
                //Log.i("UserLatLng", pickUpLat + ", " + pickUpLng);
            }
        }


        if (dropOffName.equals("Anywhere")){
            dropOffLat = "null";
            dropOffLng = "null";
        }

        Log.i("PickUpDate", pickUpDate);

        if (pickUpDate.equals("Any Date")){
            Log.i("AnyDate","Here");
            pickUpDate = "";
        }





        MainActivity.sharedPreferences.edit().putString("JsonLoads","").apply();

        String url = "https://irishcouriernetwork.herokuapp.com/api/loads/?format=json&q='{ \"dist1\":" + dist1 + ", \"lat1\":"+ pickUpLat +", \"long1\":" + pickUpLng + " , \"dist2\":" + dist2 + ", \"lat2\": " + dropOffLat + ", \"long2\":"+ dropOffLng + ", \"pick_up_date\": \""+ pickUpDate  +"\" }'";

        Log.i("URLHere",url);


        RecyclerViewActivityDownloadTask recyclerViewDownLoadTask = new RecyclerViewActivityDownloadTask(LoadsRecyclerView.this);

        recyclerViewDownLoadTask.execute(url);


    }























    public String parseMonthAndDay(int value){

        String newValue;

        if(value < 10){
           newValue = "0" + String.valueOf(value);
        }else{
            newValue = String.valueOf(value);
        }

        return newValue;

    }









    public void changePickupOrDropOffLocation(View view){



        Intent intent = new Intent(this, PickUpDropOffActivity.class);
        intent.putExtra("viewTag",String.valueOf(view.getTag()));
        startActivity(intent);
        finish();



    }



    public void resetDatePicker(View view){

        pickUpDateET.setText("Any Date");

        MainActivity.sharedPreferences.edit().putString("pickUpDate", "Any Date").apply();

        downloadNewLoads("resetPickUpDate");

    }






    public void getUserLocation() {
        //Log.i("Here1", "Right");

        //i am required to ask if i have permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            //just being double sure that the app is listening for location changes so calling it from her as well
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            //Log.i("Here2", "Right");

            if (usersLocation != null) {
                //updating the cave lat long and accuracy with the users location
                //updateCaveLatLongLocation(usersLocation.getLatitude(), usersLocation.getLongitude(), usersLocation);

                //Log.i("Here3", "Right");
            }else{

                //Toast.makeText(this, "GPS location is not available", Toast.LENGTH_SHORT).show();

                //prompt user to turn the GPS On if it is turned off
                promptUserToTurnOnGPS();


            }
        }

    }


    public void promptUserToTurnOnGPS(){

        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}



        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("GPS network is not enabled");
            dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }else {
            Toast.makeText(this, "There seems to be a problem!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener); // TODO: 03/09/2018 maybe this will work

    }




    public void refreshLoads(View view){

        Log.i("RefreshLoads", "Activated");


        downloadNewLoads("");


    }

    //this method parses the search radius from the spinner adn removes the "km" and "+" and then converts it to meters
    public String parseSearchRadius(String searchRadius){

        if (!searchRadius.equals("All Loads")) {

            String parsedSearchRadius = searchRadius.replace("km", "").replace("+", "").replace(" ","");

            String searchRadiusInMeters = String.valueOf(Integer.valueOf(parsedSearchRadius));

            return searchRadiusInMeters;

        }else{
            return "null";
        }
    }


    public void updateLoadsV1(){


        String response = MainActivity.sharedPreferences.getString("JsonLoads","");

//            if (response.equals("[]")){
//                noLoadsTV.setVisibility(View.VISIBLE);
//            }else{
//                noLoadsTV.setVisibility(View.INVISIBLE);
//            }


            Log.i("SharedPrefResponse", response );

        recyclerViewArrayList.clear();

        try {

                                JSONArray loads = new JSONArray(response);

                                for (int i = 0; i < loads.length(); i++) {

                                    JSONObject jsonLoad = loads.getJSONObject(i);

                                    //Log.i("LoadJson " + String.valueOf(i), String.valueOf(jsonLoad));

                                    //Log.i("URL", jsonLoad.getString("url"));


                                    LoadClass loadObject = new LoadClass();

                                    loadObject.setDataBaseId(jsonLoad.getInt("id"));
                                    loadObject.setLoadId(jsonLoad.getString("load_id"));
                                    loadObject.setUrl(jsonLoad.getString("url"));
                                    loadObject.setUser(jsonLoad.getString("user"));

                                    loadObject.setClientName(jsonLoad.getString("client_name"));
                                    loadObject.setClientPhoneNumber(jsonLoad.getString("client_phone_number"));
                                    loadObject.setPaymentTerms(jsonLoad.getString("payment_terms"));

                                    loadObject.setPickUpLocation(jsonLoad.getString("pick_up_location"));
                                    loadObject.setPickUpLocationLat(jsonLoad.getString("pick_up_location_lat"));
                                    loadObject.setPickUpLocationLng(jsonLoad.getString("pick_up_location_lng"));

                                    loadObject.setDropOffLocation(jsonLoad.getString("drop_off_location"));
                                    loadObject.setDropOffLocationLat(jsonLoad.getString("drop_off_location_lat"));
                                    loadObject.setDropOffLocationLng(jsonLoad.getString("drop_off_location_lng"));

                                    loadObject.setPickUpTime(jsonLoad.getString("pick_up_time"));
                                    loadObject.setDropOffTime(jsonLoad.getString("drop_off_time"));

                                    loadObject.setVehicleType(jsonLoad.getString("vehicle_type"));

                                    loadObject.setWeight(jsonLoad.getString("weight"));

                                    //this is the distance from the the pickup location to the user specified location one
                                    loadObject.setDistFromPickUpToLoc1(jsonLoad.getString("dist_from_pickup_to_loc1"));

                                    loadObject.setNotes(jsonLoad.getString("notes"));

                                    loadObject.setUpdated(jsonLoad.getString("updated"));

                                    loadObject.setTimeStamp(jsonLoad.getString("timestamp"));


                                    //Log.i("LoadObject Get", loadObject.getLoadId());

                                    loadsArrayList.add(loadObject);

                                    recyclerViewArrayList.add(new RecyclerViewItem(loadObject));

                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



        loadsRecyclerViewAdapter = new LoadsRecyclerViewAdapter(recyclerViewArrayList,LoadsRecyclerView.this);

        recyclerView.setAdapter(loadsRecyclerViewAdapter);



    }










    public void updateLoads(String response){


        //String response = MainActivity.sharedPreferences.getString("JsonLoads","");

//            if (response.equals("[]")){
//                noLoadsTV.setVisibility(View.VISIBLE);
//            }else{
//                noLoadsTV.setVisibility(View.INVISIBLE);
//            }


        recyclerViewArrayList.clear();

        try {

            JSONArray loads = new JSONArray(response);

            for (int i = 0; i < loads.length(); i++) {

                JSONObject jsonLoad = loads.getJSONObject(i);

                //Log.i("LoadJson " + String.valueOf(i), String.valueOf(jsonLoad));

                //Log.i("URL", jsonLoad.getString("url"));


                LoadClass loadObject = new LoadClass();

                loadObject.setDataBaseId(jsonLoad.getInt("id"));
                loadObject.setLoadId(jsonLoad.getString("load_id"));
                loadObject.setUrl(jsonLoad.getString("url"));
                loadObject.setUser(jsonLoad.getString("user"));

                loadObject.setClientName(jsonLoad.getString("client_name"));
                loadObject.setClientPhoneNumber(jsonLoad.getString("client_phone_number"));
                loadObject.setPaymentTerms(jsonLoad.getString("payment_terms"));

                loadObject.setPickUpLocation(jsonLoad.getString("pick_up_location"));
                loadObject.setPickUpLocationLat(jsonLoad.getString("pick_up_location_lat"));
                loadObject.setPickUpLocationLng(jsonLoad.getString("pick_up_location_lng"));

                loadObject.setDropOffLocation(jsonLoad.getString("drop_off_location"));
                loadObject.setDropOffLocationLat(jsonLoad.getString("drop_off_location_lat"));
                loadObject.setDropOffLocationLng(jsonLoad.getString("drop_off_location_lng"));

                loadObject.setPickUpTime(jsonLoad.getString("pick_up_time"));
                loadObject.setDropOffTime(jsonLoad.getString("drop_off_time"));

                loadObject.setVehicleType(jsonLoad.getString("vehicle_type"));

                loadObject.setWeight(jsonLoad.getString("weight"));

                //this is the distance from the the pickup location to the user specified location one
                loadObject.setDistFromPickUpToLoc1(jsonLoad.getString("dist_from_pickup_to_loc1"));

                loadObject.setNotes(jsonLoad.getString("notes"));

                loadObject.setUpdated(jsonLoad.getString("updated"));

                loadObject.setTimeStamp(jsonLoad.getString("timestamp"));


                //Log.i("LoadObject Get", loadObject.getLoadId());


                //loadsArrayList.add(loadObject);

                recyclerViewArrayList.add(new RecyclerViewItem(loadObject));

            }




        } catch (JSONException e) {
            e.printStackTrace();
        }


        //loadsRecyclerViewAdapter = new LoadsRecyclerViewAdapter(recyclerViewArrayList,LoadsRecyclerView.this);

        //recyclerView.setAdapter(loadsRecyclerViewAdapter);



    }





























//    public void populateLoadsTwo(){
//
//        ArrayList<String> loadId = new ArrayList<>();
//
//        loadId.add("ongwori");
//        loadId.add("lnborbn");
//        loadId.add("osibnwbnfp");
//
//        for (int i = 0; i < loadId.size(); i++) {
//
//            LoadClass load = new LoadClass();
//
//            load.setLoadId(loadId.get(i));
//
//            recyclerViewArrayList.add(new RecyclerViewItem(load) );
//
//        }
//    }










    //this should be done inside of async
    //this should be done inside of async
    //this should be done inside of async
    //this should be done inside of async
    //this should be done inside of async

//    public void populateLoads(){
//
//        // Instantiate the RequestQueue.
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url ="https://irishcouriernetwork.herokuapp.com/api/loads/?format=json";
//
//        // Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//
//                        JSONArray loads = new JSONArray(response);
//
//                        for (int i = 0; i < loads.length(); i++) {
//
//                            JSONObject jsonLoad = loads.getJSONObject(i);
//
//                            LoadClass loadObject = new LoadClass();
//
//                            loadObject.setDataBaseId(jsonLoad.getInt("id"));
//                            loadObject.setLoadId(jsonLoad.getString("load_id"));
//                            loadObject.setUrl(jsonLoad.getString("url"));
//                            loadObject.setUser(jsonLoad.getString("user"));
//
//                            loadObject.setClientName(jsonLoad.getString("client_name"));
//                            loadObject.setClientPhoneNumber(jsonLoad.getString("client_phone_number"));
//                            loadObject.setPaymentTerms(jsonLoad.getString("payment_terms"));
//
//                            loadObject.setPickUpLocation(jsonLoad.getString("pick_up_location"));
//                            loadObject.setPickUpLocationLat(jsonLoad.getString("pick_up_location_lat"));
//                            loadObject.setPickUpLocationLng(jsonLoad.getString("pick_up_location_lng"));
//
//                            loadObject.setDropOffLocation(jsonLoad.getString("drop_off_location"));
//                            loadObject.setDropOffLocationLat(jsonLoad.getString("drop_off_location_lat"));
//                            loadObject.setDropOffLocationLng(jsonLoad.getString("drop_off_location_lng"));
//
//                            loadObject.setPickUpTime(jsonLoad.getString("pick_up_time"));
//                            loadObject.setDropOffTime(jsonLoad.getString("drop_off_time"));
//
//                            loadObject.setVehicleType(jsonLoad.getString("vehicle_type"));
//
//                            loadObject.setWeight(jsonLoad.getString("weight"));
//
//                            loadObject.setNotes(jsonLoad.getString("notes"));
//
//                            loadObject.setUpdated(jsonLoad.getString("updated"));
//
//                            loadObject.setTimeStamp(jsonLoad.getString("timestamp"));
//
//                            recyclerViewArrayList.add(new RecyclerViewItem(loadObject));
//
//                        }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //textView.setText("That didn't work!");
//            }
//        });
//
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
//
//    }







//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.search_menu_items,menu);
//
//        MenuItem menuItem = menu.findItem(R.id.action_search);
//
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
//
//        searchView.setOnQueryTextListener(this);
//
//        return super.onCreateOptionsMenu(menu);
//    }



    @Override
    protected void onResume() {
        super.onResume();

        loadsRecyclerViewAdapter = new LoadsRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(loadsRecyclerViewAdapter);


    }


//    @Override
//    public boolean onQueryTextSubmit(String query) {
//
//        return false;
//    }

//    @Override
//    public boolean onQueryTextChange(String newText) {
//
//        newText = newText.toLowerCase();
//
//
//        ArrayList<RecyclerViewItem> newList = new ArrayList<>();
//
//        for(RecyclerViewItem item : recyclerViewArrayList){
//
//            String sellerName = item.getSellerName().toLowerCase();
//
//            if (sellerName.contains(newText)) {
//
//                newList.add(item);
//
//            }
//
//        }
//        Log.i("Size", String.valueOf(recyclerViewArrayList.size()));
//
//        recyclerViewAdapter.setFilter(newList);
//
//
//
//
//        return true;
//
//    }






















    private class RecyclerViewActivityDownloadTask extends AsyncTask<String, Integer, ArrayList<RecyclerViewItem>> {

        private Context mContext;



        private ProgressDialog progressDialog;

        public RecyclerViewActivityDownloadTask(Context context){

            mContext = context;

        }



        @Override
        protected void onPreExecute() {


            Log.i("On Pre Execute", "Fired");

            progressDialog = new ProgressDialog(LoadsRecyclerView.this);
            progressDialog.setCancelable(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setProgressStyle(0);
            progressDialog.setMax(100);
            //progressDialog.setMessage(RecyclerViewActivity.this.getString("Downloading Loads"));
            progressDialog.setMessage("Downloading Loads");

            //make a button
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected ArrayList<RecyclerViewItem> doInBackground(String... urls) {


            Log.i("Downloading", "Yessssss");

            publishProgress(1);


            publishProgress(3);

//            // Instantiate the RequestQueue.
//            RequestQueue queue = Volley.newRequestQueue(LoadsRecyclerView.this);
//            //String url ="https://irishcouriernetwork.herokuapp.com/api/loads/?format=json";
//
//            String url = urls[0];
//            //Log.i("URLS", url);
//
//            // Request a string response from the provided URL.
//            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//
//                           MainActivity.sharedPreferences.edit().putString("JsonLoads","").apply();
//
//                           MainActivity.sharedPreferences.edit().putString("JsonLoads",response ).apply();
//
//                            Log.i("ResponseFromICN", response);
//
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    //textView.setText("That didn't work!");
//                    Log.i("Oh Dear", "There were a load of errors trying to connect to ICN");
//                }
//            });
//
//            // Add the request to the RequestQueue.
//            queue.add(stringRequest);


            publishProgress(50);

            publishProgress(100);






            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            String url = urls[0];



             StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.length() > 0) {

                        noLoadsTV.setVisibility(View.INVISIBLE);


                        recyclerViewArrayList.clear();

                        updateLoads(response);

                        loadsRecyclerViewAdapter.notifyDataSetChanged();


                        if (response.equals("[]")){
                            noLoadsTV.setVisibility(View.VISIBLE);
                        }

                    }










                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                }
            });

            requestQueue.add(stringRequest);




















            return null;
        }


        @Override
        protected void onPostExecute(ArrayList<RecyclerViewItem> recyclerViewArrayList2) {
            super.onPostExecute(recyclerViewArrayList2);

            progressDialog.dismiss();

            Log.i("On Post Execute", "Fired");

            //recyclerViewArrayList.clear();

//            final Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    updateLoads();
//
//                }
//            }, 1000);



        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            //updating the progress bar
            progressDialog.setProgress(values[0]);

        }
    }






}



