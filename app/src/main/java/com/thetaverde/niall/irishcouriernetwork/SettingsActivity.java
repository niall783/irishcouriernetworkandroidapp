package com.thetaverde.niall.irishcouriernetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {


    TextView homeAddressTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setTitle("Settings");

        String homeAddress = MainActivity.sharedPreferences.getString("HomeAddress","No Address Provided");
        homeAddressTV = findViewById(R.id.homeAddressTV);
        homeAddressTV.setText(homeAddress);



    }



    public void enterHomeAddress(View view){

        Intent intent = new Intent(this,EnterAddressActivity.class);
        startActivity(intent);
        //finish();

    }


}
