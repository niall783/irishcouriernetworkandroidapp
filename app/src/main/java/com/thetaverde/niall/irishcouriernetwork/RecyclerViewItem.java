package com.thetaverde.niall.irishcouriernetwork;

public class RecyclerViewItem {



     /*private CaveDTO caveDTO;
    private String caveName;
    private String caveCountry;*/

    private LoadClass loadClass;


    private String loadId;

    private String clientNameTV;

    private String timeStamp;
    private String dropOfTimeTV;
    private String dropOfLocTV;
    private String pickUpTimeTV;
    private String pickUpLocTV;
    private String vanTypeTV;

    private String distFromPickUpToLoc1TV;




    //this part below is so that i can populate the recyclerViewItemArrayList using CaveDTO object directly - mb

    public RecyclerViewItem(LoadClass load){

        this.setClientNameTV(load.getClientName());

        this.setLoadId(load.getLoadId());
        this.setTimeStamp(load.getTimeStamp());

        this.setDropOfTimeTV(load.getDropOffTime());
        this.setDropOfLocTV(load.getDropOffLocation());

        this.setPickUpTimeTV(load.getPickUpTime());
        this.setPickUpLocTV(load.getPickUpLocation());

        this.setVanTypeTV(load.getVehicleType());

        this.setDistFromPickUpToLoc1TV(load.getDistFromPickUpToLoc1() + " km away");


        this.setLoadClass(load);

    }




    public String getLoadId() {
        return loadId;
    }

    public void setLoadId(String loadId) {
        this.loadId = loadId;
    }


    public LoadClass getLoadClass() {
        return loadClass;
    }

    public void setLoadClass(LoadClass loadClass) {
        this.loadClass = loadClass;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDropOfTimeTV() {
        return dropOfTimeTV;
    }

    public void setDropOfTimeTV(String dropOfTimeTV) {
        this.dropOfTimeTV = dropOfTimeTV;
    }

    public String getDropOfLocTV() {
        return dropOfLocTV;
    }

    public void setDropOfLocTV(String dropOfLocTV) {
        this.dropOfLocTV = dropOfLocTV;
    }

    public String getPickUpTimeTV() {
        return pickUpTimeTV;
    }

    public void setPickUpTimeTV(String pickUpTimeTV) {
        this.pickUpTimeTV = pickUpTimeTV;
    }

    public String getPickUpLocTV() {
        return pickUpLocTV;
    }

    public void setPickUpLocTV(String pickUpLocTV) {
        this.pickUpLocTV = pickUpLocTV;
    }

    public String getVanTypeTV() {
        return vanTypeTV;
    }

    public void setVanTypeTV(String vanTypeTV) {
        this.vanTypeTV = vanTypeTV;
    }

    public String getClientNameTV() {
        return clientNameTV;
    }

    public void setClientNameTV(String clientNameTV) {
        this.clientNameTV = clientNameTV;
    }

    public String getDistFromPickUpToLoc1TV() {
        return distFromPickUpToLoc1TV;
    }

    public void setDistFromPickUpToLoc1TV(String distFromCurrentLocationTV) {
        this.distFromPickUpToLoc1TV = distFromCurrentLocationTV;
    }
}
