package com.thetaverde.niall.irishcouriernetwork;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.time.OffsetDateTime;

public class LoadActivity extends AppCompatActivity {


    Intent intent;

    TextView loadIdTV;

    TextView clientNameTV;
    TextView clientPhoneNumberTV;

    TextView dropOfTimeTV;
    TextView dropOfLocTV;
    TextView pickUpTimeTV;
    TextView pickUpLocTV;
    TextView vanTypeTV;

    TextView weightTV;


    TextView notesTV;

    TextView updatedTimeTV;
    TextView timeStampTV;


    LoadClass loadObject;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);


        intent = getIntent();

        loadObject = (LoadClass) intent.getSerializableExtra("LoadObject");

        setTitle("Load ID: " + loadObject.getLoadId());




        loadIdTV = (TextView) findViewById(R.id.loadIdTV);

        clientNameTV = (TextView) findViewById(R.id.clientNameTV);
        clientPhoneNumberTV = (TextView) findViewById(R.id.clientPhoneNumberTV);


        pickUpLocTV = (TextView) findViewById(R.id.pickUpLocTV);
        pickUpTimeTV = (TextView) findViewById(R.id.pickUpTimeTV);

        dropOfTimeTV = (TextView) findViewById(R.id.dropOfTimeTV);
        dropOfLocTV = (TextView) findViewById(R.id.dropOfLocTV);

        vanTypeTV = (TextView) findViewById(R.id.vanTypeTV);

        weightTV = (TextView) findViewById(R.id.weightTV);

        notesTV = (TextView) findViewById(R.id.notesTV);


        timeStampTV = (TextView) findViewById(R.id.timeStampTV);
        updatedTimeTV = (TextView) findViewById(R.id.updatedTimeTV);



        loadIdTV.setText(loadObject.getLoadId());

        clientNameTV.setText(loadObject.getClientName());
        clientPhoneNumberTV.setText(loadObject.getClientPhoneNumber());


        pickUpLocTV.setText(loadObject.getPickUpLocation());
        pickUpTimeTV.setText(parseDateTime(loadObject.getPickUpTime()));

        dropOfTimeTV.setText(parseDateTime(loadObject.getDropOffTime()));
        dropOfLocTV.setText(loadObject.getDropOffLocation());

        vanTypeTV.setText(loadObject.getVehicleType());

        weightTV.setText(loadObject.getWeight());

        notesTV.setText(loadObject.getNotes());


        timeStampTV.setText(parseDateTime(loadObject.getTimeStamp()));
        updatedTimeTV.setText(parseDateTime(loadObject.getUpdated()));












    }


    public void driveToPickUp(View view){

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com").appendPath("maps").appendPath("dir").appendPath("").appendQueryParameter("api", "1")
                .appendQueryParameter("destination", loadObject.getPickUpLocationLat() + "," + loadObject.getPickUpLocationLng());
        String url = builder.build().toString();
        Log.d("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }




    public void driveToDropOf(View view){

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com").appendPath("maps").appendPath("dir").appendPath("").appendQueryParameter("api", "1")
                .appendQueryParameter("destination", loadObject.getDropOffLocationLat() + "," + loadObject.getDropOffLocationLng());
        String url = builder.build().toString();
        Log.d("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }







    public String parseDateTime(String dateTime){


        OffsetDateTime odt = OffsetDateTime.parse(dateTime);


        String newTime = odt.getHour() + ":" + odt.getMinute();

        //when the minute is less that 10 is prints 13:5 instead of 13:05
        int minute = odt.getMinute();
        if (minute < 10){

            newTime = odt.getHour() + ":0" + odt.getMinute();

        }



        String newDate = odt.getDayOfMonth() + " " + odt.getMonth();


        return newTime + " | " + newDate;

    }




}
