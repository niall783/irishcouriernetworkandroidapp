package com.thetaverde.niall.irishcouriernetwork;

import java.io.Serializable;

public class LoadClass implements Serializable {


    private int dataBaseId;
    private String loadId;
    private String url;
    private String user;

    private String clientName;
    private String clientPhoneNumber;
    private String paymentTerms;

    private String pickUpLocation;
    private String pickUpLocationLat;
    private String pickUpLocationLng;

    private String dropOffLocation;
    private String dropOffLocationLat;
    private String dropOffLocationLng;

    private String pickUpTime;
    private String dropOffTime;

    private String vehicleType;

    private String weight;

    private String notes;

    private String distFromPickUpToLoc1;

    private String updated;

    private String timeStamp;








    public String getLoadId() {
        return loadId;
    }

    public void setLoadId(String loadId) {
        this.loadId = loadId;
    }


    public int getDataBaseId() {
        return dataBaseId;
    }

    public void setDataBaseId(int dataBaseId) {
        this.dataBaseId = dataBaseId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getPickUpLocationLat() {
        return pickUpLocationLat;
    }

    public void setPickUpLocationLat(String pickUpLocationLat) {
        this.pickUpLocationLat = pickUpLocationLat;
    }

    public String getPickUpLocationLng() {
        return pickUpLocationLng;
    }

    public void setPickUpLocationLng(String pickUpLocationLng) {
        this.pickUpLocationLng = pickUpLocationLng;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public String getDropOffLocationLat() {
        return dropOffLocationLat;
    }

    public void setDropOffLocationLat(String dropOffLocationLat) {
        this.dropOffLocationLat = dropOffLocationLat;
    }

    public String getDropOffLocationLng() {
        return dropOffLocationLng;
    }

    public void setDropOffLocationLng(String dropOffLocationLng) {
        this.dropOffLocationLng = dropOffLocationLng;
    }

    public String getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpTime(String pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public String getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffTime(String dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDistFromPickUpToLoc1() {
        return distFromPickUpToLoc1;
    }

    public void setDistFromPickUpToLoc1(String distFromCurrentLocation) {
        this.distFromPickUpToLoc1 = distFromCurrentLocation;
    }
}
