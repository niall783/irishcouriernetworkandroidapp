package com.thetaverde.niall.irishcouriernetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;
import java.util.List;


public class PickUpDropOffActivity extends AppCompatActivity {


    Intent intent;

    String viewTag;

    TextView pickUpDropOffTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up_drop_off);


        intent = getIntent();

        viewTag = intent.getStringExtra("viewTag");

        pickUpDropOffTV = findViewById(R.id.pickUpDropOffTV);

        if (viewTag.equals("drop_off")){
            setTitle("Drop Off Location");
            pickUpDropOffTV.setText("Anywhere");
        }else if (viewTag.equals("pick_up")){
            setTitle("Pick Up Location");
            pickUpDropOffTV.setText("My Location");
        }




        final String TAG = "placeautocomplete";

        String apikey = "AIzaSyCN8OG6iT0KzmorNs-50MJdK0Mduv1yt_E";

        // Initialize Places.
        Places.initialize(getApplicationContext(), apikey);
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                //txtView.setText(place.getName()+","+place.getId());
                //Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                //Log.i(TAG, "Place: " + place.getName() + ", "+ place.getAddress()+ "  " + place.getLatLng()+  " " + place.getId());

            LatLng latLng = place.getLatLng();

            if (latLng != null) {
                String lat = String.valueOf(latLng.latitude);
                String lng = String.valueOf(latLng.longitude);
                String name = String.valueOf(place.getName());
                String address = place.getAddress();


                if (viewTag.equals("drop_off")) {
                    MainActivity.sharedPreferences.edit().putString("DropOffLat", lat).apply();
                    MainActivity.sharedPreferences.edit().putString("DropOffLng", lng).apply();
                    MainActivity.sharedPreferences.edit().putString("DropOffName", name).apply();
                    //MainActivity.sharedPreferences.edit().putString("DropOffAddress", address).apply();
                }else if (viewTag.equals("pick_up")){
                    MainActivity.sharedPreferences.edit().putString("PickUpLat", lat).apply();
                    MainActivity.sharedPreferences.edit().putString("PickUpLng", lng).apply();
                    MainActivity.sharedPreferences.edit().putString("PickUpName", name).apply();
                    //MainActivity.sharedPreferences.edit().putString("PickUpAddress", address).apply();
                }
            }

            returnToLoads();



            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });















    }


    public void  pickUpDropOffButton(View view){

        if (viewTag.equals("drop_off")) {
            MainActivity.sharedPreferences.edit().putString("DropOffLat", "null").apply();
            MainActivity.sharedPreferences.edit().putString("DropOffLng", "null").apply();
            MainActivity.sharedPreferences.edit().putString("DropOffName", "Anywhere").apply();
        }else if (viewTag.equals("pick_up")){
            MainActivity.sharedPreferences.edit().putString("PickUpLat", "null").apply();
            MainActivity.sharedPreferences.edit().putString("PickUpLng", "null").apply();
            MainActivity.sharedPreferences.edit().putString("PickUpName", "My Location").apply();
        }

        returnToLoads();
    }







    public void returnToLoads(){

        Intent intent = new Intent(this, LoadsRecyclerView.class);
        intent.putExtra("sourceActivity", "PickUpDropOffActivity" );
        startActivity(intent);
        finish();

    }












}
